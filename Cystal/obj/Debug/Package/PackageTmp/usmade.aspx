﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="usmade.aspx.vb" Inherits="Cystal.usmade1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
        AutoDataBind="True" GroupTreeImagesFolderUrl="" Height="1202px" 
        ReportSourceID="USMade" ToolbarImagesFolderUrl="" 
        ToolPanelView="ParameterPanel" ToolPanelWidth="200px" Width="1104px" 
        HasCrystalLogo="False" />
    <CR:CrystalReportSource ID="USMade" runat="server">
        <Report FileName="USMade.rpt">
        </Report>
    </CR:CrystalReportSource>
</asp:Content>
