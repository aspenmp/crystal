﻿Imports CrystalDecisions.Shared

Imports CrystalDecisions.CrystalReports.Engine

Public Class finishedgoods
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        configureCRYSTALREPORT()
    End Sub
    Private Sub configureCRYSTALREPORT()
        Dim myConnectionInfo As New ConnectionInfo()

        myConnectionInfo.DatabaseName = "ProdReport"
        myConnectionInfo.UserID = "ProdReport"
        myConnectionInfo.Password = "ProdReport"
        setDBLOGONforREPORT(myConnectionInfo)
    End Sub

    Private Sub setDBLOGONforREPORT(ByVal myconnectioninfo As ConnectionInfo)
        Dim mytableloginfos As New TableLogOnInfos()
        mytableloginfos = CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In mytableloginfos
            myTableLogOnInfo.ConnectionInfo = myconnectioninfo
        Next

    End Sub
End Class