﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SalesReport.aspx.vb" Inherits="Cystal.SalesReport" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
<table>
<tr>
<td>
    UserID :
    <asp:TextBox runat="server" ID="UserID"></asp:TextBox>
    <asp:Button runat="server" Text="Run Report" ID="RunReport" ></asp:Button>(test it with UserID: 1234 or 3456)
</td>
</tr>

<tr>
<td>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
    AutoDataBind="True" GroupTreeImagesFolderUrl="" Height="100%" 
    ReportSourceID="CrystalReportSource1" ToolbarImagesFolderUrl="" 
    ToolPanelView="None" ToolPanelWidth="200px" Width="100%" />

<CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
    <Report FileName="TestReport.rpt">
<%--  <Parameters>

        <CR:ControlParameter ControlID="UserID" ConvertEmptyStringToNull="False" DefaultValue=""
            Name="@UserID" PropertyName="Text" ReportName="" />

    </Parameters> --%>
    </Report>
</CR:CrystalReportSource>

</td>
</tr>
</table>


</asp:Content>
