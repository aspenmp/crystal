﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Public Class SalesReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CrystalReportViewer1.Visible = False
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("sa", "@$penMP", "slider", "AspenNet")
    End Sub

    Protected Sub RunReport_Click(sender As Object, e As EventArgs) Handles RunReport.Click

        Dim reportDocument As ReportDocument = New ReportDocument()
        Dim paramField As ParameterField = New ParameterField()
        Dim paramFields As ParameterFields = New ParameterFields()
        Dim paramDiscreteValue As ParameterDiscreteValue = New ParameterDiscreteValue()

        paramField.Name = "@UserID"
        paramDiscreteValue.Value = UserID.Text.ToString()
        paramField.CurrentValues.Add(paramDiscreteValue)
        paramFields.Add(paramField)
        CrystalReportViewer1.ParameterFieldInfo = paramFields

        reportDocument.Load(Server.MapPath("TestReport.rpt"))

        CrystalReportViewer1.Visible = True
    End Sub

End Class