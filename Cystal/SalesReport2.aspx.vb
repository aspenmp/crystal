﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Public Class SalesReport2
    Inherits System.Web.UI.Page

    Private Sub SetDBConnection()
        Dim rootWebConfig As System.Configuration.Configuration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~")
        Dim connString As System.Configuration.ConnectionStringSettings
        If rootWebConfig.ConnectionStrings.ConnectionStrings.Count > 0 Then
            connString = rootWebConfig.ConnectionStrings.ConnectionStrings("AspenNet")
            If connString IsNot Nothing Then
                Console.WriteLine("DB connection string = ""{0}""", connString.ConnectionString)

                Dim builder As New System.Data.Common.DbConnectionStringBuilder()

                builder.ConnectionString = connString.ToString()

                Dim zServer As String = TryCast(builder("Data Source"), String)
                Dim zDatabase As String = TryCast(builder("Initial Catalog"), String)
                Dim zUsername As String = TryCast(builder("User ID"), String)
                Dim zPassword As String = TryCast(builder("Password"), String)

                'CrystalReportSource1.ReportDocument.SetDatabaseLogon("sa", "qburst", "localhost\mssql", "AspenNet")
                CrystalReportSource1.ReportDocument.SetDatabaseLogon(zUsername, zPassword, zServer, zDatabase)
                CrystalReportSource1.ReportDocument.VerifyDatabase()
                CrystalReportSource1.ReportDocument.Refresh()

            Else
                Console.WriteLine("No DB connection string")
            End If
        End If
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        SetDBConnection()
        If (Not IsPostBack) Then
            CrystalReportViewer1.Visible = False

        Else
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


    End Sub

    Protected Sub RunReport_Click(sender As Object, e As EventArgs) Handles RunReport.Click
        CrystalReportViewer1.ReportSourceID = "CrystalReportSource1"
        SetDBConnection()
        CrystalReportViewer1.Visible = True

    End Sub


End Class